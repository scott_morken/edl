<?php
return [
    'concrete' => [
        \App\Storage\Eloquent\Address::class => [
            'model' => [
                'impl' => \App\Models\Eloquent\Address::class,
            ],
        ],
        \App\Storage\Eloquent\Group::class   => [
            'model' => [
                'impl' => \App\Models\Eloquent\Group::class,
            ],
        ],
        \App\Storage\Config\Parser::class    => [
            'model' => [
                'impl' => \App\Models\Config\Parser::class,
            ],
        ],
        \App\Storage\Eloquent\Token::class   => [
            'model' => [
                'impl' => \App\Models\Eloquent\Token::class,
            ],
        ],
    ],
    'contract' => [
        \App\Contracts\Storage\Address::class => \App\Storage\Eloquent\Address::class,
        \App\Contracts\Storage\Group::class   => \App\Storage\Eloquent\Group::class,
        \App\Contracts\Storage\Parser::class  => \App\Storage\Config\Parser::class,
        \App\Contracts\Storage\Token::class   => \App\Storage\Eloquent\Token::class,
    ],
];
