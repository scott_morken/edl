<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 9:11 AM
 */
return [
    'exclusions' => [
        '140.198.0.0/16',
    ],
    'refreshers' => [
        'pan' => [
            'backend_options' => [
                'http_errors'     => false,
                'connect_timeout' => 5,
                'verify'          => false,
            ],
            'endpoint'        => env(
                'PAN_ENDPOINT',
                'https://localhost/apitype=op&cmd=<request><system><external-list><refresh><type><ip><name>NAME</name></ip></type></refresh></external-list></system></request>&key='
            ),
        ],
    ],
];
