<?php

use Smorken\Auth\Proxy\Http\Controllers\Admin\User\Controller;

return [
    'guest' => [],
    'auth'  => [],

    'role-manage' => [],
    'role-admin'  => [
        [
            'name'   => 'Addresses',
            'action' => [\App\Http\Controllers\Admin\Address\Controller::class, 'index'],
            'children',
        ],
        [
            'name'     => 'Groups',
            'action'   => [\App\Http\Controllers\Admin\Group\Controller::class, 'index'],
            'children' => [],
        ],
        [
            'name'   => 'Tokens',
            'action' => [\App\Http\Controllers\Admin\Token\Controller::class, 'index'],
            'children',
        ],
        [
            'name'     => 'Users',
            'action'   => [Controller::class, 'index'],
            'children' => [],
        ],
    ],
];
