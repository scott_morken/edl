<textarea name="{{ $name }}" id="{{ $id??$name }}" class="form-control {{ $classes??'' }}"
          placeholder="{{ $placeholder??'' }}"
          maxlength="{{ $maxlength??2048 }}"
          rows="{{ $rows??3 }}">{{ $value??old($name, (isset($model) && $model->$name) ? $model->$name : null) }}</textarea>
