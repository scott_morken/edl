<div class="form-check {{ $wrapper_classes??'' }}">
    <input type="hidden" name="{{ $name }}" value="0">
    @include('_preset.input._checkbox')
    @include('_preset.input._label', ['id' => ($id??$name).'-'.($value??1), 'label_classes' => 'form-check-label'])
</div>
