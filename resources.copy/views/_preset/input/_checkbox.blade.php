<input type="checkbox" class="form-check-input {{ $classes??'' }}" name="{{ $name }}"
       id="{{ ($id??$name).'-'.($value??1) }}"
       value="{{ $value??1 }}" {{ ($checked??old($name, (isset($model) && $model->$name) ? $model->$name : false))?'checked':'' }}>
