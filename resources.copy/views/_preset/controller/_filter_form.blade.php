<?php $filtered = 'border border-success'; ?>
<div class="card mt-2 mb-2">
    <div class="card-body">
        <form class="form-inline" method="get">
            <div class="form-group mb-2 mr-2">
                <label for="NAME" class="sr-only">NAME</label>
                <input type="text" class="form-control {{ $filter->NAME ? $filtered : null }}" id="NAME"
                       value="{{ $filter->NAME }}" maxlength="255"
                       placeholder="NAME">
            </div>
            <button type="submit" class="btn btn-primary mb-2 mr-2">Filter</button>
            <a href="{{ action([$controller, 'index']) }}" class="btn btn-outline-danger mb-2" title="Reset filter">Reset</a>
        </form>
    </div>
</div>
