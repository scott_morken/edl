<a href="{{ action([$controller, 'delete'], $params) }}" class="text-danger"
   title="Delete {{ $model->getKey() }}">{{ $text ?? 'delete' }}</a>
