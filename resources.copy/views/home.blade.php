@extends('layouts.app')
@section('content')
    @if ($counts && count($counts))
        @foreach ($counts as $count)
            <div>{{ $count->active ? 'Active' : 'Inactive' }}: {{ $count->active_count }}</div>
        @endforeach
    @else
        <div class="text-muted">No addresses found.</div>
    @endif
@stop
