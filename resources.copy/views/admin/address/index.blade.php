<?php
/**
 * @var \Illuminate\Support\Collection $models
 */
?>
@extends('layouts.app')
@section('content')
    <div>
        <a href="{{ action([$controller, 'refresh'], $filter->toArray()) }}"
           title="Refresh backends" class="pull-right">
            Refresh Backends
        </a>
    </div>
    @include('_preset.controller._title', ['title' => 'IP Administration'])
    @includeIf('admin.address._filter_form')
    @include('_preset.controller.index_actions._create')

    @if ($models && count($models))
        <table class="table table-striped">
            <thead>
            <tr>
                <td>ID</td>
                <td>IP Address</td>
                <td>Group</td>
                <td>Description</td>
                <td>Active</td>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($models as $model)
                <?php $params = array_merge([$model->getKeyName() => $model->getKey()],
                    isset($filter) ? $filter->except(['page']) : []); ?>
                <tr>
                    <td>@include('_preset.controller.index_actions._view', ['value' => $model->getKey()])</td>
                    <td>{{ $model->address }}</td>
                    <td>{{ $model->group ? $model->group->description : $model->group_id }}</td>
                    <td>{{ $model->description }}</td>
                    <td>{{ $model->active ? 'Yes' : 'No' }}</td>
                    <td>
                        @include('_preset.controller.index_actions._update_delete')
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if (method_exists($models, 'links'))
            {{ $models->appends(isset($filter)?$filter->except(['page']):[])->links() }}
        @endif
    @else
        <div class="text-muted">No records found.</div>
    @endif
@endsection
