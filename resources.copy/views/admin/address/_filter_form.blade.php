<?php $filtered = 'border border-success'; ?>
<div class="card mt-2 mb-2">
    <div class="card-body">
        <form class="form-inline" method="get">
            <div class="form-group mb-2 mr-2">
                <label for="address" class="sr-only">IP Address</label>
                <input type="text" class="form-control {{ $filter->address ? $filtered : null }}" id="address"
                       value="{{ $filter->address }}" maxlength="255"
                       placeholder="IP Address">
            </div>
            <div class="form-group mb-2 mr-2">
                <label for="group_id" class="sr-only">Group</label>
                <select name="group_id" class="form-control {{ $filter->group_id && $filter->group_id !== '*' ? $filtered : null }}">
                    <option value="*">-- Any Group --</option>
                    @foreach ($groups->pluck('description', 'id') as $v => $text)
                        <option
                            value="{{ $v }}" {{ $filter->group_id && $filter->group_id == $v ? 'selected' : ($group_id && $group_id == $v ? 'selected' : '') }}>
                            {{ $text }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group mb-2 mr-2">
                <label for="active" class="sr-only">Active</label>
                <?php $actives = ['0' => 'Inactive', '1' => 'Active']; ?>
                <select name="active" class="form-control {{ $filter->active ? $filtered : null }}">
                    <option value="">-- Any Active --</option>
                    @foreach ($actives as $v => $text)
                        <option
                            value="{{ $v }}" {{ strlen($filter->active) && $filter->active == $v ? 'selected' : ''}}>
                            {{ $text }}
                        </option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-primary mb-2 mr-2">Filter</button>
            <a href="{{ action([$controller, 'index']) }}" class="btn btn-outline-danger mb-2" title="Reset filter">Reset</a>
        </form>
    </div>
</div>
