@extends('layouts.app')
<?php
$params = array_merge([$model->getKeyName() => $model->getKey()], isset($filter) ? $filter->all() : []);
$first_col = 'col-sm-2';
$second_col = 'col-sm-10';
?>
@section('content')
    @include('_preset.controller._to_index')
    @include('_preset.controller._title', ['title' => 'IP Administration'])
    <h5 class="mb-2">View record [{{ $model->getKey() }}]</h5>
    <div class="row mb-4">
        <div class="col">
            <a href="{{ action([$controller, 'update'], $params) }}" title="Update {{ $model->getKey() }}"
               class="btn btn-primary btn-block">Update</a>
        </div>
        <div class="col">
            <a href="{{ action([$controller, 'delete'], $params) }}" title="Delete {{ $model->getKey() }}"
               class="btn btn-danger btn-block">Delete</a>
        </div>
    </div>
    <div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">ID</div>
            <div class="{{ $second_col }}">{{ $model->id }}</div>
        </div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">Group</div>
            <div class="{{ $second_col }}">{{ $model->group ? $model->group->description : $model->group_id }}</div>
        </div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">IP Address</div>
            <div class="{{ $second_col }}">{{ $model->address }}</div>
        </div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">Active</div>
            <div class="{{ $second_col }}">{{ $model->active ? 'Yes' : 'No' }}</div>
        </div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">Remove At</div>
            <div class="{{ $second_col }}">{{ $model->remove_at ?? 'never' }}</div>
        </div>
    </div>
    <pre>{{ a2str($model->cidr->asArray()) }}</pre>
@endsection
