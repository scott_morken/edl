<?php $update = isset($update) ?: false; ?>
@if ($update)
    @include('_preset.input.g_input_ro_plaintext', ['name' => 'address', 'title' => 'IP Address'])
@else
    @include('_preset.input.g_input', ['name' => 'address', 'title' => 'IP Address'])
@endif
<div class="form-group">
    <label for="group_id">Group</label>
    <select name="group_id" class="form-control">
        @foreach ($groups->pluck('description', 'id') as $v => $text)
            <option
                value="{{ $v }}" {{ $model->id && $model->id == $v ? 'selected' : ($group_id && $group_id == $v ? 'selected' : '') }}>
                {{ $text }}
            </option>
        @endforeach
    </select>
</div>
@include('_preset.input.g_input', ['name' => 'description', 'title' => 'Description'])
<div class="form-group">
    <label for="remove_at">Expire (remove) After</label>
    @if ($model->id && $model->remove_at)
        <input type="text" name="remove_at" value="{{ $model->remove_at }}" class="form-control" maxlength="20">
    @else
        <select name="remove_at" class="form-control">
            <option value="">None</option>
            <option value="{{ date('Y-m-d H:i:s', strtotime('+1 hour')) }}">+1 hour</option>
            <option value="{{ date('Y-m-d H:i:s', strtotime('+12 hours')) }}">+12 hours</option>
            <option value="{{ date('Y-m-d H:i:s', strtotime('+1 day')) }}">+1 day</option>
            <option value="{{ date('Y-m-d H:i:s', strtotime('+1 week')) }}">+1 week</option>
        </select>
    @endif
</div>
@include('_preset.input.g_check_bool', ['name' => 'active', 'title' => 'Active'])
