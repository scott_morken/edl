<?php $filtered = 'border border-success'; ?>
<div class="card mt-2 mb-2">
    <div class="card-body">
        <form class="form-inline" method="get">
            <div class="form-group mb-2 mr-2">
                <label for="ip" class="sr-only">IP</label>
                <input type="text" name="ip" value="{{ $filter->ip }}" placeholder="IP Address" class="form-control  {{ $filter->ip ? $filtered : null }}">
            </div>
            <div class="form-group mb-2 mr-2">
                <label for="host" class="sr-only">Host</label>
                <input type="text" name="host" value="{{ $filter->host }}" placeholder="Host" class="form-control {{ $filter->host ? $filtered : null }}">
            </div>
            <button type="submit" class="btn btn-primary mb-2 mr-2">Filter</button>
            <a href="{{ action([$controller, 'index']) }}" class="btn btn-outline-danger mb-2" title="Reset filter">Reset</a>
        </form>
    </div>
</div>
