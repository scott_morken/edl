<?php
/**
 * @var \Illuminate\Support\Collection $models
 */
?>
@extends('layouts.app')
@section('content')
    @include('_preset.controller._title', ['title' => 'Token Administration'])
    @includeIf('admin.token._filter_form')
    @include('_preset.controller.index_actions._create')

    @if ($models && count($models))
        <table class="table table-striped">
            <thead>
            <tr>
                <td>ID</td>
                <td>IP Address</td>
                <td>Token</td>
                <td>Host</td>
                <td>Description</td>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($models as $model)
                <?php $params = array_merge([$model->getKeyName() => $model->getKey()],
                    isset($filter) ? $filter->except(['page']) : []); ?>
                <tr>
                    <td>@include('_preset.controller.index_actions._view', ['value' => $model->getKey()])</td>
                    <td>{{ $model->ip }}</td>
                    <td>{{ sprintf('...%s', substr($model->token, -10)) }}</td>
                    <td>{{ $model->host }}</td>
                    <td>{{ sprintf('%s%s', substr($model->descr, 0, 10), strlen($model->descr) > 10 ? '...' : '') }}</td>
                    <td>
                        @include('_preset.controller.index_actions._update_delete')
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if (method_exists($models, 'links'))
            {{ $models->appends(isset($filter)?$filter->except(['page']):[])->links() }}
        @endif
    @else
        <div class="text-muted">No records found.</div>
    @endif
@endsection
