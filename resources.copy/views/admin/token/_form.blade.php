<?php
$update = isset($update) ?: false;
$token = $model->token ? : (isset($token) ? $token : null);
?>
@include('_preset.input.g_input', ['name' => 'ip', 'title' => 'IP'])
@if ($update)
    @include('_preset.input.g_input_ro_plaintext', ['name' => 'token', 'title' => 'Token', 'value' => $token])
@else
    @include('_preset.input.g_input', ['name' => 'token', 'title' => 'Token', 'value' => $token])
@endif
@include('_preset.input.g_input', ['name' => 'host', 'title' => 'Host'])
@include('_preset.input.g_input', ['name' => 'descr', 'title' => 'Description'])
