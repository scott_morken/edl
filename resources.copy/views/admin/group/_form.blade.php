@include('_preset.input.g_select', ['name' => 'parser', 'title' => 'Parser', 'items' => $parsers->pluck('name', 'id')])
@include('_preset.input.g_input', ['name' => 'description', 'title' => 'Description'])
@include('_preset.input.g_check_bool', ['name' => 'active', 'title' => 'Active'])
