@extends('layouts.app')
@include('_preset.controller.view', ['title' => 'Group Administration'])
@section('content')
    <div>
        Url: {{ action('Address\Controller@index', ['id' => $model->id, 'token' => 'TOKEN']) }}
    </div>
    <div class="card">
        <div class="card-header"><h4 class="card-title">Active Addresses</h4></div>
        <div class="card-body">
            <pre>{{ $model->parseAddresses() }}</pre>
        </div>
    </div>
@append
