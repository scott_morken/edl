<?php
/**
 * @var \Illuminate\Support\Collection $models
 * @var \App\Contracts\Models\Training $model
 * @var \Smorken\Support\Contracts\Filter $filter
 */
?>
@extends('layouts.app')
@include('_preset.controller.index', ['title' => 'Group Administration'])
