@auth
    <?php $menus = Menu::getMenusByKey('auth'); ?>
    @if ($menus)
        @foreach($menus as $menu)
            <?php $active = Menu::isActiveChain($controller, $menu); ?>
            <?php $sub = $active && count($menu->children) ? $menu->children : null; ?>
            @include('layouts.menus._menu_item')
        @endforeach
    @endif

@endauth
