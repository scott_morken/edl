@foreach (session()->all() as $k => $v)
    @if (\Illuminate\Support\Str::startsWith($k, 'flash:'))
        <div class="alert alert-{{ substr($k, 6) }} mb-1">
            <div class="container">{{ $v }}</div>
        </div>
    @endif
@endforeach
