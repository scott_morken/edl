<div id="modal" class="modal fade" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="modal-title">
                    @if (isset($title))
                        {{ $title }}
                    @endif
                </h4>
            </div>
            <div class="modal-body" id="modal-body">
                <p>
                    @if (isset($body))
                        {!! $body !!}
                    @endif
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
