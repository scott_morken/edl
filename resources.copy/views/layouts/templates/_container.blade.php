<main class="py-4 container">
    @yield('content')
</main>
<footer class="py-4 container">
    <div class="text-center">
        <div class="text-muted"><small>&copy; {{ date('Y') }} Phoenix College</small>
            @if (file_exists(public_path('images/footer.png')))
                <div>
                    <a href="https://www.maricopa.edu" title="Maricopa Community Colleges">
                        <img src="{{ asset('images/footer.png') }}" alt="A Maricopa Community College"
                             height="30">
                    </a>
                </div>
            @endif
        </div>
    </div>
    @yield('footer')
</footer>
@include('layouts._partials._modal')
