ARG PHPDOTVER=18.04

FROM php:${PHPDOTVER}-cli-alpine

ARG XDEBUG_VERSION=2.7.2
ARG PHPDOTVER=7.3

RUN apk add --no-cache --virtual build-deps $PHPIZE_DEPS git && \
    docker-php-ext-install pcntl && \
    pecl install xdebug-${XDEBUG_VERSION} && \
    docker-php-ext-enable xdebug

# Composer + PHP tools
RUN \
  php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
  php composer-setup.php --install-dir=/usr/local/bin --filename=composer && \
  php -r "unlink('composer-setup.php');"

COPY ./composer.json /tmp/composer.json

RUN \
    cd /tmp && \
    composer update --no-interaction && \
    ln -s /tmp/vendor/bin/phpunit /usr/local/bin/phpunit && \
    ln -s /tmp/vendor/bin/phing /usr/local/bin/phing && \
    ln -s /tmp/vendor/bin/phpcs /usr/local/bin/phpcs && \
    ln -s /tmp/vendor/bin/phpcbf /usr/local/bin/phpcbf && \
    ln -s /tmp/vendor/bin/phpcpd /usr/local/bin/phpcpd && \
    ln -s /tmp/vendor/bin/phpmd /usr/local/bin/phpmd && \
    ln -s /tmp/vendor/bin/phploc /usr/local/bin/phploc && \
    apk del build-deps

# XDebug port
EXPOSE 9000

VOLUME /app
WORKDIR /app
