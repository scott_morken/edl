publish: composer_up npm_install webpack tar_all tar ## Build dependencies and create tar.gz files

publish_dev: composer_up_dev npm_install webpack_dev ## Build dependencies (with dev)

chown: ## Change the ownership to $(USER_ID):$(GROUP_ID) of $(APP_PATH)
	$(BIN_DOCKER) exec -it $(CONTAINER_PHP) find /app ! -path "*/docker.*" ! -path "*/node_modules*" -exec chown $(USER_ID):$(GROUP_ID) {} \;

artisan: ## Run an artisan command in the running PHP container
	$(BIN_DOCKER) exec -it --user $(USER_ID):$(GROUP_ID) $(CONTAINER_PHP) php artisan $(command)

composer_up_dev: ## Composer update with require-dev
	USER_ID=$(USER_ID) GROUP_ID=$(GROUP_ID) APP_PATH=$(APP_PATH) /bin/bash ./composer update

composer_up: ## Composer update without require-dev
	USER_ID=$(USER_ID) GROUP_ID=$(GROUP_ID) APP_PATH=$(APP_PATH) /bin/bash ./composer update --no-dev

composer_install: ## Composer install
	USER_ID=$(USER_ID) GROUP_ID=$(GROUP_ID) APP_PATH=$(APP_PATH) /bin/bash ./composer update composer install

npm_install: ## NPM install
	USER_ID=$(USER_ID) GROUP_ID=$(GROUP_ID) APP_PATH=$(APP_PATH) NODE_VERSION=$(NODE_VERSION) /bin/bash ./node npm install

gulp: ## NPM install
	USER_ID=$(USER_ID) GROUP_ID=$(GROUP_ID) APP_PATH=$(APP_PATH) NODE_VERSION=$(NODE_VERSION) /bin/bash ./node /app/node_modules/.bin/gulp

webpack_dev: ## Webpack build (dev)
	USER_ID=$(USER_ID) GROUP_ID=$(GROUP_ID) APP_PATH=$(APP_PATH) NODE_VERSION=$(NODE_VERSION) /bin/bash ./node npm run dev

webpack: ## Webpack build (no dev)
	USER_ID=$(USER_ID) GROUP_ID=$(GROUP_ID) APP_PATH=$(APP_PATH) NODE_VERSION=$(NODE_VERSION) /bin/bash ./node npm run prod

webpack_watch: ## Start the webpack watcher
	USER_ID=$(USER_ID) GROUP_ID=$(GROUP_ID) APP_PATH=$(APP_PATH) NODE_VERSION=$(NODE_VERSION) /bin/bash ./node npm run watch

tar: ## Create tar.gz excluding resources and config
	tar $(EXCLUDE_TAR) --exclude="./resources" --exclude="./config" -czf $(PROJECT).min.tar.gz -C $(APP_PATH) .

tar_all: ## Create tar.gz with resources and config
	tar $(EXCLUDE_TAR) -czf $(PROJECT).all.tar.gz -C $(APP_PATH) .

tar_docker: ## Create tar.gz including docker files
	tar --exclude=".git" --exclude="./.idea" --exclude="./bower_components" --exclude="./node_modules" --exclude="logs/*" --exclude="*.tar.gz" -czf $(PROJECT).docker.tar.gz -C $(APP_PATH) .

zip: ## Create zip file
	cd $(APP_PATH) && zip -FSr $(PROJECT).all.zip ./* $(EXCLUDE_ZIP) && cd - && mv $(APP_PATH)/$(PROJECT).all.zip .

copy_logo: ## Copy branded logo file
	cp $(LOGO_FILE) $(APP_PATH)/public/images/logo.png

copy_favicon: ## Copy branded favicon
	cp $(ICO_FILE) $(APP_PATH)/public/favicon.ico
