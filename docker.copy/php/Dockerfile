ARG UBUNTU_VERSION=18.04

FROM ubuntu:${UBUNTU_VERSION}

ARG UBUNTU_VERSION=18.04
ARG UBUNTU_NAME=bionic
ARG XDEBUG_VERSION=2.7.2
ARG PHPDOTVER=7.3
ARG WWW_USER=www-data
ARG WWW_GROUP=www-data
ARG USER_ID=1000
ARG GROUP_ID=1000
ARG MSSQL_INSTALL=1
ARG XDEBUG_INSTALL=1

RUN \
  export DEBIAN_FRONTEND=noninteractive && \
  apt-get update && \
  apt-get install -y wget curl gnupg2

RUN \
  export DEBIAN_FRONTEND=noninteractive && \
  echo "deb http://ppa.launchpad.net/ondrej/php/ubuntu ${UBUNTU_NAME} main" | tee /etc/apt/sources.list.d/php.list && \
  apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E5267A6C && \
  apt-get update && \
  apt-get install -y php${PHPDOTVER}-cli php${PHPDOTVER}-fpm php${PHPDOTVER}-mysql php${PHPDOTVER}-curl php${PHPDOTVER}-gd \
  php${PHPDOTVER}-xml php${PHPDOTVER}-bcmath php${PHPDOTVER}-mbstring \
  mysql-client

# PHP module configs
COPY ./conf/mods-available/ /etc/php/${PHPDOTVER}/mods-available/

RUN \
  if [ "$MSSQL_INSTALL" = 1 ]; then \
  echo 'Installing SQL Server libraries' && \
  curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - && \
  curl https://packages.microsoft.com/config/ubuntu/${UBUNTU_VERSION}/prod.list | tee /etc/apt/sources.list.d/mssql.list && \
  export DEBIAN_FRONTEND=noninteractive && \
  apt-get update && \
  apt-get install -y libssl1.0-dev libssl1.0 && \
  ACCEPT_EULA=Y apt-get install -y build-essential php${PHPDOTVER}-dev php-pear unixodbc-dev mssql-tools && \
  pecl install sqlsrv && \
  pecl install pdo_sqlsrv && \
  phpenmod sqlsrv && \
  phpenmod pdo_sqlsrv; \
  fi

RUN \
  if [ "$XDEBUG_INSTALL" = 1 ]; then \
  echo "Installing Xdebug version $XDEBUG_VERSION" && \
  export DEBIAN_FRONTEND=noninteractive && \
  apt-get update && \
  apt-get install -y build-essential php${PHPDOTVER}-dev php-pear && \
  wget http://xdebug.org/files/xdebug-${XDEBUG_VERSION}.tgz -O xdebug.tgz && \
  tar xzf xdebug.tgz && \
  rm -f xdebug.tgz && \
  cd xdebug-*/ && \
  phpize && \
  ./configure --with-php-config=/usr/bin/php-config && \
  make && \
  export TEST_PHP_ARGS='-n' && \
  make test && \
  make install && \
  cd .. && \
  rm -Rf xdebug-*/ && \
  phpenmod xdebug; \
  fi

# cleanup
RUN \
  apt-get remove --purge -y build-essential php${PHPDOTVER}-dev php-pear unixodbc-dev && \
  apt-get clean && \
  apt-get autoremove -y && \
  rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN \
  phpenmod mailcatcher

# Fix php session dir and error log
RUN \
  mkdir -p /var/lib/php/sessions && \
  chown -R ${WWW_USER}:${WWW_GROUP} /var/lib/php/sessions && \
  chmod 0777 -R /var/lib/php/sessions/ && \
  mkdir -p /var/log/php/ && \
  touch /var/log/php/error.log && \
  chmod 0755 /var/log/php/error.log && \
  chown -R ${WWW_USER}:${WWW_GROUP} /var/log/php/ && \
  chmod 0777 -R /var/log/php/ && \
  rm -f /etc/php/${PHPDOTVER}/fpm/pool.d/*


COPY ./conf/php.ini /etc/php/${PHPDOTVER}/fpm/conf.d/99-php.ini
COPY ./conf/php.ini /etc/php/${PHPDOTVER}/cli/conf.d/99-php.ini
COPY ./conf/fpm.conf /etc/php/${PHPDOTVER}/fpm/pool.d/00-general.conf
COPY ./conf/fpm-pool.conf /etc/php/${PHPDOTVER}/fpm/pool.d/01-pool.conf


# Create php pid location
RUN \
    mkdir -p /run/php

RUN \
   groupmod -g ${GROUP_ID} ${WWW_GROUP} && \
   usermod -u ${USER_ID} ${WWW_USER} && \
   mkdir -p /app && \
   chown -R ${USER_ID}:${GROUP_ID} /app && \
   chmod -R 6775 /app

# XDebug port
EXPOSE 9000

VOLUME /app
WORKDIR /app

COPY ./entrypoint.sh /entrypoint.sh

RUN \
  echo "/usr/sbin/php-fpm${PHPDOTVER} -F" >> /entrypoint.sh && chmod u+x /entrypoint.sh

CMD ["/entrypoint.sh"]
