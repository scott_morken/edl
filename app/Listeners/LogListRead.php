<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/18/18
 * Time: 7:04 AM
 */

namespace App\Listeners;

use Psr\Log\LoggerInterface;

class LogListRead
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $log;

    public function __construct(LoggerInterface $log)
    {
        $this->log = $log;
    }

    public function handle(\App\Events\ListRead $event)
    {
        $template = '%s Token: %s Host: %s Status: %d';
        $this->log->channel('access')
                  ->info(sprintf($template, $event->ip, $event->token ?: 'null', $event->host ?: 'null',
                      $event->status));
    }
}
