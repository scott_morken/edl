<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/18/18
 * Time: 7:02 AM
 */

namespace App\Events;

class ListRead extends Event
{

    public $ip;

    public $token;

    public $host;

    public $status;

    public function __construct($ip, $token, $host, $status)
    {
        $this->ip = $ip;
        $this->token = $token;
        $this->host = $host;
        $this->status = $status;
    }
}
