<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 1:59 PM
 */

namespace App\Models\Config;

use Smorken\Model\Config;

class Parser extends Config implements \App\Contracts\Models\Parser
{

    /**
     * Identifier to load config
     *
     * @var string
     */
    protected $config_key = 'parsers';

    /**
     * @return \App\Contracts\Parsers\Parser
     */
    public function getInstance()
    {
        return new $this->id;
    }
}
