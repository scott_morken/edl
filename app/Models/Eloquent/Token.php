<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/21/18
 * Time: 12:32 PM
 */

namespace App\Models\Eloquent;

class Token extends Base implements \App\Contracts\Models\Token
{

    protected $fillable = [
        'ip',
        'token',
        'host',
        'descr',
    ];

    protected $rules = [
        'ip'    => 'required|ip',
        'token' => 'required|max:255',
        'host'  => 'max:128',
        'descr' => 'max:128',
    ];

    public function __toString()
    {
        return sprintf('%d: %s - ...%s', $this->id, $this->ip, substr($this->token, -10));
    }

    public function scopeDefaultOrder($query)
    {
        return $query->orderBy('ip');
    }

    public function scopeHostIs($query, $host)
    {
        return $query->where('host', '=', $host);
    }

    public function scopeIpIs($query, $ip)
    {
        return $query->where('ip', '=', $ip);
    }

    public function scopeTokenIs($query, $token)
    {
        return $query->where('token', '=', $token);
    }
}
