<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 2:09 PM
 */

namespace App\Models\Eloquent;

use Illuminate\Support\Facades\App;

class Group extends Base implements \App\Contracts\Models\Group
{

    protected $fillable = ['parser', 'description', 'active'];

    protected $rules = [
        'parser'      => [
            'required',
            'max:128',
        ],
        'description' => 'required|max:128',
        'active'      => 'boolean',
    ];

    /**
     * @var \App\Contracts\Parsers\Parser
     */
    protected $parser_inst;

    public function __toString()
    {
        return sprintf('%d: %s', $this->id, $this->description);
    }

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    /**
     * @return \App\Contracts\Parsers\Parser
     */
    public function instantiateParser()
    {
        if (!$this->parser_inst) {
            $this->parser_inst = App::make($this->parser);
        }
        return $this->parser_inst;
    }

    /**
     * @return mixed
     */
    public function parseAddresses()
    {
        return $this->instantiateParser()
                    ->parse($this->addresses);
    }

    public function scopeActiveAddresses($query)
    {
        return $query->with([
            'addresses' => function ($sq) {
                return $sq->activeIs(1)
                          ->orderDefault();
            },
        ]);
    }

    public function scopeActiveIs($query, $active)
    {
        return $query->where('active', '=', $active);
    }

    public function scopeDefaultOrder($query)
    {
        return $this->scopeOrderDefault($query);
    }

    public function scopeOrderDefault($query)
    {
        return $query->orderBy('active', 'desc')
                     ->orderBy('description');
    }
}
