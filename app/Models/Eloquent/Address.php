<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 8:39 AM
 */

namespace App\Models\Eloquent;

use App\Services\Ip\Cidr;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class Address extends Base implements \App\Contracts\Models\Address
{

    protected $fillable = ['group_id', 'address', 'description', 'active', 'remove_at'];

    public function __toString()
    {
        return sprintf('%d: %s - %s', $this->id, $this->address, ($this->active ? 'Y' : 'N'));
    }

    public function getCidrAttribute()
    {
        return new Cidr(Arr::get($this->attributes, 'address'));
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function scopeActiveIs($query, $active)
    {
        return $query->where('active', '=', $active);
    }

    public function scopeAddressLike($query, $address)
    {
        $address = $address.'%';
        return $query->where('address', 'LIKE', $address);
    }

    public function scopeDefaultOrder($query)
    {
        return $this->scopeOrderDefault($query);
    }

    public function scopeDefaultWiths($query)
    {
        return $query->with('group');
    }

    public function scopeGroupIdIs($query, $id)
    {
        return $query->where('group_id', '=', $id);
    }

    public function scopeOrderDefault($query)
    {
        return $query->orderBy('active', 'desc')
                     ->orderBy(DB::raw('(address + 0), length(address), address'));
    }

    public function scopeRemove($query)
    {
        $date = date('Y-m-d H:i:s');
        return $query->where(function ($sq) use ($date) {
            $sq->whereNotNull('remove_at')
               ->where('remove_at', '<', $date);
        });
    }

    public function validationRules(array $override = []): array
    {
        return array_replace([
            'group_id'    => 'required|int',
            'address'     => [
                'sometimes',
                'required',
                new \App\Services\Validators\Address(new Cidr(), Config::get('addresses.exclusions', [])),
            ],
            'active'      => 'boolean',
            'description' => 'string|max:128',
        ], $override);
    }
}
