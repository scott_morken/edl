<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 2:39 PM
 */

namespace App\Services\Validators;

use Illuminate\Contracts\Validation\Rule;

class Parser implements Rule
{

    /**
     * @var \App\Contracts\Storage\Parser
     */
    protected $parser;

    public function __construct(\App\Contracts\Storage\Parser $parser)
    {
        $this->parser = $parser;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must be in the allowed parser list.';
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return in_array($value, $this->parser->allowedToArray());
    }
}
