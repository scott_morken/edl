<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 10:01 AM
 */

namespace App\Services\Validators;

use App\Contracts\Ip\Cidr;

abstract class AddressBase
{

    /**
     * @var \App\Contracts\Ip\Cidr
     */
    protected $cidr;

    public function __construct(Cidr $cidr)
    {
        $this->cidr = $cidr;
    }

    /**
     * @return \App\Contracts\Ip\Cidr
     */
    public function getCidr()
    {
        return $this->cidr;
    }
}
