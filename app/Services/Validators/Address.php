<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 10:07 AM
 */

namespace App\Services\Validators;

use App\Contracts\Ip\Cidr;
use App\Services\Ip\CidrException;
use Illuminate\Contracts\Validation\Rule;

class Address extends AddressBase implements Rule
{

    protected $message = 'The :attribute must be a valid subnet mask.';

    protected $exclusions = [];

    public function __construct(\App\Contracts\Ip\Cidr $cidr, array $exclusions = [])
    {
        $this->exclusions = $exclusions;
        parent::__construct($cidr);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        try {
            $ip = $this->getCidr()
                       ->newInstance($value);
            if ($this->excluded($ip)) {
                return false;
            }
            if (!$this->isPublic($ip)) {
                $this->message = 'The :attribute must be a public IP.';
                return false;
            }
        } catch (CidrException $ce) {
            return false;
        }
        return true;
    }

    protected function excluded(Cidr $ip)
    {
        foreach ($this->exclusions as $exip) {
            try {
                if ($ip->inSubnet($exip)) {
                    $this->message = 'The :attribute must not be in an excluded subnet [' . $exip . '].';
                    return $exip;
                }
            } catch (CidrException $ce) {
                $this->message = 'Invalid exclusion subnet [' . $exip . '].';
                return true;
            }
        }
        return false;
    }

    protected function isPublic(Cidr $ip)
    {
        return filter_var($ip->getIp(), FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE);
    }
}
