<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 12:53 PM
 */

namespace App\Services\Refreshers;

use App\Contracts\Refreshers\Refresh;

class Factory implements \App\Contracts\Refreshers\Factory
{

    protected $refreshers = [];

    /**
     * @param                                   $key
     * @param \App\Contracts\Refreshers\Refresh $refresh
     * @return mixed
     */
    public function add($key, Refresh $refresh)
    {
        $this->refreshers[$key] = $refresh;
    }

    /**
     * @return array
     */
    public function all()
    {
        return $this->refreshers;
    }

    /**
     * @param $key
     * @return \App\Contracts\Refreshers\Refresh
     */
    public function get($key)
    {
        if (array_key_exists($key, $this->refreshers)) {
            return $this->refreshers[$key];
        }
        throw new \InvalidArgumentException("$key is not a valid refresher.");
    }
}
