<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 12:41 PM
 */

namespace App\Services\Refreshers;

use App\Contracts\Refreshers\Refresh;
use GuzzleHttp\Client;

class None implements Refresh
{

    /**
     * @return null
     */
    public function getBackend()
    {
        return null;
    }

    public function refresh()
    {
        return true;
    }
}
