<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 12:41 PM
 */

namespace App\Services\Refreshers;

use App\Contracts\Refreshers\Refresh;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;

class Pan implements Refresh
{

    /**
     * @var \GuzzleHttp\Client
     */
    protected $client;

    /**
     * @var array
     */
    protected $config;

    public function __construct(Client $client, array $config)
    {
        $this->client = $client;
    }

    /**
     * @return \GuzzleHttp\Client
     */
    public function getBackend()
    {
        return $this->client;
    }

    public function refresh()
    {
        $config = $this->getConfigItem('backend_options', []);
        $url = $this->getConfigItem(['endpoint']);
        $response = $this->getBackend()
                         ->get($url, $config);
        return $response->getStatusCode() === 200;
    }

    protected function getConfigItem($key, $default = null)
    {
        return Arr::get($this->config, $key, $default);
    }
}
