<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/14/17
 * Time: 2:04 PM
 */

namespace App\Services\Ip;

use Illuminate\Support\Arr;

class Cidr implements \App\Contracts\Ip\Cidr
{

    /**
     * CIDR notation
     *
     * @var string
     */
    protected $subnet;

    protected $attributes = [
        'ip'          => null,
        'netmask'     => null,
        'netmask_int' => null,
        'network'     => null,
        'broadcast'   => null,
    ];

    /**
     * Cidr constructor.
     *
     * @param string|null $subnet CIDR notation 127.0.0.1/32
     */
    public function __construct($subnet = null)
    {
        if ($subnet) {
            $this->subnet = $subnet;
            $this->init($subnet);
            $this->validate();
            $this->build();
        }
    }

    public function __toString()
    {
        return sprintf('%s/%s', $this->getIp(), $this->getNetmaskInt());
    }

    public function asArray($as_long = false)
    {
        return [
            'ip'        => $this->getIp($as_long),
            'netmask'   => $this->getNetmask($as_long),
            'network'   => $this->getNetwork($as_long),
            'broadcast' => $this->getBroadcast($as_long),
        ];
    }

    /**
     * @param bool $as_long
     * @return string|int
     */
    public function getBroadcast($as_long = false)
    {
        return $this->get('broadcast', $as_long);
    }

    public function getFirst($as_long = false)
    {
        $netlong = $this->getNetwork(true);
        $firstlong = $netlong + 1;
        return $as_long ? $firstlong : long2ip($firstlong);
    }

    /**
     * @param bool $as_long
     * @return string|int
     */
    public function getIp($as_long = false)
    {
        return $this->get('ip', $as_long);
    }

    public function getLast($as_long = false)
    {
        $bclong = $this->getBroadcast(true);
        $lastlong = $bclong - 1;
        return $as_long ? $lastlong : long2ip($lastlong);
    }

    /**
     * @param bool $as_long
     * @return int|string
     */
    public function getNetmask($as_long = false)
    {
        return $this->get('netmask', $as_long);
    }

    public function getNetmaskInt()
    {
        return $this->get('netmask_int');
    }

    /**
     * @param bool $as_long
     * @return string|int
     */
    public function getNetwork($as_long = false)
    {
        return $this->get('network', $as_long);
    }

    /**
     * @param bool $as_long
     * @return \Generator
     */
    public function getRange($as_long = false)
    {
        $start = $this->getNetwork(true) + 1;
        $stop = $this->getBroadcast(true);
        for ($ip = $start; $ip < $stop; $ip++) {
            yield ($as_long ? $ip : long2ip($ip));
        }
    }

    /**
     * @param $subnet
     * @return bool
     */
    public function inSubnet($subnet)
    {
        $in = $this->newInstance($subnet);
        $origip = $this->getIp(true);
        $start = $in->getNetwork(true);
        $stop = $in->getBroadcast(true);
        return $origip > $start && $origip < $stop;
    }

    /**
     * @param $subnet
     * @return static
     */
    public function newInstance($subnet)
    {
        return new static($subnet);
    }

    /**
     * @return bool
     * @throws \App\Services\Ip\CidrException
     */
    public function validate()
    {
        $ip = $this->getIp();
        $mask = $this->getNetmask();
        if ($ip && $mask) {
            return true;
        }
        throw new CidrException("{$this->subnet} is not a valid subnet mask.");
    }

    protected function build()
    {
        $ipl = $this->getIp(true);
        $maskl = $this->getNetmask(true);
        $this->set('network', long2ip($ipl & $maskl));
        $lastl = $this->getNetwork(true) | $this->getInverseMaskLong($maskl);
        $last = long2ip($lastl);
        if ($last !== '255.255.255.255') {
            $this->set('broadcast', $last);
        } else {
            $this->set('broadcast', $this->getNetwork());
        }
    }

    protected function get($key, $as_long = false)
    {
        if ($key) {
            $v = Arr::get($this->attributes, $key, null);
            return $v && $as_long ? ip2long($v) : $v;
        }
        return null;
    }

    protected function getInverseMaskLong($mask_long)
    {
        $inverse = ~$mask_long;
        return $inverse;
    }

    protected function getNetmaskFromCidr($mask)
    {
        return bindec(str_repeat('1', $mask) . str_repeat('0', 32 - $mask));
    }

    protected function init($subnet)
    {
        $parts = explode('/', $subnet);
        if (filter_var($parts[0], FILTER_VALIDATE_IP)) {
            $this->set('ip', $parts[0]);
        }
        if (count($parts) === 2) {
            $mask = (int)$parts[1];
            $this->initMask($mask);
        }
    }

    protected function initMask($mask)
    {
        if ($mask > 0 && $mask <= 32) {
            $netmask = long2ip($this->getNetmaskFromCidr($mask));
            if ($netmask !== '0.0.0.0') {
                $this->set('netmask_int', $mask);
                $this->set('netmask', $netmask);
            }
        }
    }

    protected function set($key, $value)
    {
        if ($key) {
            Arr::set($this->attributes, $key, $value);
        }
    }
}
