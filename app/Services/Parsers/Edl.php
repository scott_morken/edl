<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 11:55 AM
 */

namespace App\Services\Parsers;

use App\Contracts\Parsers\Parser;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class Edl implements Parser
{

    /**
     * @param \Illuminate\Support\Collection $collection
     * @return string
     */
    public function parse(Collection $collection)
    {
        $key = 'parser.edl';
        return Cache::remember(
            $key,
            10,
            function () use ($collection) {
                $template = '%s %d: %s';
                $out = [];
                foreach ($collection as $address) {
                    $out[] = sprintf($template, $address->address, $address->id, $address->description ?: 'none');
                }
                return implode(PHP_EOL, $out);
            }
        );

    }
}
