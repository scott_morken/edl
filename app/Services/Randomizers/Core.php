<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 8:28 AM
 */

namespace App\Services\Randomizers;

use App\Contracts\Randomizer;

class Core implements Randomizer
{

    /**
     * @param int $len
     * @return string
     */
    public function generate($len = 64)
    {
        return bin2hex(random_bytes($len));
    }
}
