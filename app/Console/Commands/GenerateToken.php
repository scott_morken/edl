<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/26/18
 * Time: 11:53 AM
 */

namespace App\Console\Commands;

use App\Contracts\Storage\Token;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class GenerateToken extends Command
{

    protected $signature = 'token:generate {ip} {host?}';

    protected $description = 'Generate a new token.';

    /**
     * @var \App\Contracts\Randomizer
     */
    protected $randomizer;

    /**
     * @var \App\Contracts\Storage\Token
     */
    protected $token;

    public function __construct(\App\Contracts\Randomizer $randomizer, Token $token)
    {
        $this->randomizer = $randomizer;
        $this->token = $token;
        parent::__construct();
    }

    public function handle()
    {
        $ip = $this->argument('ip');
        $host = $this->argument('host');
        $token_str = $this->randomizer->generate(32);
        $token = $this->token->create(
            ['ip' => $ip, 'host' => $host, 'token' => $token_str]
        );
        if (!$token) {
            $errs = $this->token->errors();
            foreach ($errs->all() as $k => $e) {
                $this->error($e);
            }
            return false;
        }
        Cache::flush();
        $this->line($token->token);
    }
}
