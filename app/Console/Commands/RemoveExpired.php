<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/18/18
 * Time: 9:22 AM
 */

namespace App\Console\Commands;

use App\Contracts\Storage\Address;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class RemoveExpired extends Command
{

    protected $signature = 'addresses:expire';

    protected $description = 'Remove expired addresses.';

    /**
     * @var \App\Contracts\Storage\Address
     */
    protected $address;

    public function __construct(Address $address)
    {
        $this->address = $address;
        parent::__construct();
    }

    public function handle()
    {
        $count = $this->address->removeExpired();
        Cache::flush();
        $this->line("Removed $count expired addresses.");
    }
}

