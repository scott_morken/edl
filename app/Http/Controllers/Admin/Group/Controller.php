<?php

namespace App\Http\Controllers\Admin\Group;

use App\Contracts\Storage\Group;
use App\Contracts\Storage\Parser;
use App\Http\Controllers\Traits\GroupId;
use App\Http\Requests\Admin\GroupForm;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Smorken\Ext\Controller\Traits\Full;
use Smorken\Ext\Controller\Traits\Index;

class Controller extends \App\Http\Controllers\Controller
{

    use GroupId, Full, Index {
        doSave as traitedDoSave;
        create as traitedCreate;
        update as traitedUpdate;
        redirectOnSuccessSave as traitedRedirectOnSuccessSave;
    }

    protected $base_view = 'admin.group';

    protected $subnav = 'admin';

    /**
     * @var \App\Contracts\Storage\Parser
     */
    protected $parser;

    public function __construct(Group $group, Parser $parser)
    {
        $this->setProvider($group);
        $this->parser = $parser;
        parent::__construct();
    }

    public function create(Request $request)
    {
        $parsers = $this->parser->all();
        return $this->traitedCreate($request)
                    ->with('parsers', $parsers);
    }

    public function doSave(GroupForm $request, $id = null)
    {
        return $this->traitedDoSave($request, $id);
    }

    public function update(Request $request, $id)
    {
        $parsers = $this->parser->all();
        return $this->traitedUpdate($request, $id)
                    ->with('parsers', $parsers);
    }

    protected function getAttributes(FormRequest $request)
    {
        return $request->only(['parser', 'description', 'active']);
    }

    protected function redirectOnSuccessSave(FormRequest $request, $model = null)
    {
        if ($model) {
            $this->storeGroupId($model->id);
        }
        return $this->traitedRedirectOnSuccessSave($request, $model);
    }
}
