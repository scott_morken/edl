<?php

namespace App\Http\Controllers\Admin\Token;

use App\Contracts\Randomizer;
use App\Contracts\Storage\Token;
use App\Http\Controllers\Traits\GroupId;
use App\Http\Requests\Admin\TokenForm;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Smorken\Ext\Controller\Traits\Full;
use Smorken\Ext\Controller\Traits\IndexFiltered;
use Smorken\Support\Filter;

class Controller extends \App\Http\Controllers\Controller
{
    use Full, IndexFiltered, GroupId {
        create as traitedCreate;
        doSave as traitedDoSave;
    }

    protected $base_view = 'admin.token';

    protected $subnav = 'admin';

    /**
     * @var \App\Contracts\Randomizer
     */
    protected $randomizer;

    public function __construct(Token $token, Randomizer $randomizer)
    {
        $this->setProvider($token);
        $this->randomizer = $randomizer;
        parent::__construct();
    }

    public function create(Request $request)
    {
        $token = $this->randomizer->generate(32);
        return $this->traitedCreate($request)
                    ->with('token', $token);
    }

    public function doSave(TokenForm $request, $id = null)
    {
        return $this->traitedDoSave($request, $id);
    }

    protected function getAttributes(FormRequest $request)
    {
        return $request->only(['ip', 'host', 'descr', 'token']);
    }

    protected function getFilter(Request $request)
    {
        return new Filter([
                'ip'   => $request->get('ip'),
                'host' => $request->get('host'),
            ]);
    }
}
