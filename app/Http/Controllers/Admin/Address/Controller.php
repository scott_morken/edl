<?php

namespace App\Http\Controllers\Admin\Address;

use App\Contracts\Refreshers\Factory;
use App\Contracts\Storage\Address;
use App\Contracts\Storage\Group;
use App\Http\Controllers\Traits\GroupId;
use App\Http\Requests\Admin\AddressForm;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Smorken\Ext\Controller\Traits\Full;
use Smorken\Ext\Controller\Traits\IndexFiltered;
use Smorken\Support\Filter;

class Controller extends \App\Http\Controllers\Controller
{

    use Full, IndexFiltered, GroupId {
        create as traitedCreate;
        doSave as traitedDoSave;
        index as traitedIndex;
        redirectOnSuccessSave as traitedRedirectOnSuccessSave;
        update as traitedUpdate;
    }

    protected $base_view = 'admin.address';

    protected $subnav = 'admin';

    /**
     * @var \App\Contracts\Storage\Group
     */
    protected $group;

    public function __construct(Address $address, Group $group)
    {
        $this->setProvider($address);
        $this->group = $group;
        parent::__construct();
    }

    public function create(Request $request)
    {
        $groups = $this->group->active(false);
        return $this->traitedCreate($request)
                    ->with('groups', $groups)
                    ->with('group_id', $this->retrieveGroupId());
    }

    public function doSave(AddressForm $request, $id = null)
    {
        return $this->traitedDoSave($request, $id);
    }

    public function index(Request $request)
    {
        $groups = $this->group->active(false);
        return $this->traitedIndex($request)
                    ->with('groups', $groups)
                    ->with('group_id', $this->retrieveGroupId());
    }

    public function refresh(Request $request, Factory $factory)
    {
        $filter = $this->getFilter($request);
        $results = [];
        foreach ($factory->all() as $k => $refresher) {
            $results[] = sprintf('%s: %s', $refresher->refresh());
        }
        $request->session()
                ->flash('flash:info', 'Refreshed: '.implode(', ', $results));
        return redirect()->action($this->actionArray('index'), $filter->toArray());
    }

    public function update(Request $request, $id)
    {
        $groups = $this->group->active(false);
        return $this->traitedUpdate($request, $id)
                    ->with('groups', $groups)
                    ->with('group_id', $this->retrieveGroupId());
    }

    protected function getAttributes(FormRequest $request)
    {
        $data = $request->only(['group_id', 'active', 'description', 'remove_at']);
        $address = $request->get('address');
        if ($address) {
            $data['address'] = $address;
        }
        return $data;
    }

    protected function getFilter(Request $request)
    {
        return new Filter([
            'address'  => $request->get('address'),
            'active'   => $request->get('active'),
            'group_id' => $request->get('group_id') ? $this->storeGroupId($request->get('group_id'))
                : $this->retrieveGroupId(),
        ]);
    }

    protected function redirectOnSuccessSave(FormRequest $request, $model = null)
    {
        if ($model) {
            $this->storeGroupId($model->id);
        }
        return $this->traitedRedirectOnSuccessSave($request, $model);
    }
}
