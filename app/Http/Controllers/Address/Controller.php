<?php

namespace App\Http\Controllers\Address;

use App\Contracts\Storage\Group;
use App\Contracts\Storage\Token;
use App\Events\ListRead;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class Controller extends \App\Http\Controllers\Controller
{

    /**
     * @var \App\Contracts\Storage\Group
     */
    protected $group;

    /**
     * @var \App\Contracts\Storage\Token
     */
    protected $token;

    public function __construct(Group $group, Token $token)
    {
        $this->group = $group;
        $this->token = $token;
        parent::__construct();
    }

    public function index(Request $request, $id)
    {
        $token = $this->validateToken($request);
        $group = $this->group->find($id);
        if (!$group) {
            throw new NotFoundHttpException("$id is not a valid group.");
        }
        $content = $group->parseAddresses();
        Event::dispatch(new ListRead($request->ip(), $token->token, $token->host, 1));
        return Response::make($content, 200, ['Content-Type' => 'text/plain']);
    }

    protected function validateToken(Request $request)
    {
        $token = $this->token->getToken($request->ip(), $request->get('token'), $request->get('host'));
        if (!$token) {
            Event::dispatch(new ListRead($request->ip(), $request->get('token'), $request->get('host'), 0));
            throw new UnauthorizedHttpException(sprintf('Invalid token for [%s].', $request->ip()));
        }
        return $token;
    }
}
