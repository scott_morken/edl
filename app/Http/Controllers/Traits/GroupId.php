<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 2:53 PM
 */

namespace App\Http\Controllers\Traits;

use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\App;

trait GroupId
{

    /**
     * @var \Illuminate\Contracts\Session\Session
     */
    protected $session;

    protected function getSession()
    {
        if (!$this->session) {
            $this->session = App::make('session');
        }
        return $this->session;
    }

    public function setSession(Session $session)
    {
        $this->session = $session;
    }

    protected function retrieveGroupId()
    {
        if ($this->getSession()
                 ->has('group_id')) {
            return $this->getSession()
                        ->get('group_id');
        }
    }

    protected function storeGroupId($group_id)
    {
        $this->getSession()
             ->put('group_id', $group_id);
        return $group_id;
    }
}
