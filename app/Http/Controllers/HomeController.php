<?php

namespace App\Http\Controllers;

use App\Contracts\Storage\Address;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{

    public function index(Request $request, Address $address)
    {
        $counts = $address->getActiveCounts();
        return View::make('home')
                   ->with('counts', $counts);
    }
}
