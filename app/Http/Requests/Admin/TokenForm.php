<?php

namespace App\Http\Requests\Admin;

use App\Contracts\Storage\Token;
use Illuminate\Foundation\Http\FormRequest;
use Smorken\Sanitizer\Contracts\Sanitize;
use Smorken\Sanitizer\Traits\SanitizeRequest;

class TokenForm extends FormRequest
{

    use SanitizeRequest;

    public function rules(Token $token, Sanitize $sanitize)
    {
        $this->sanitize($sanitize, $this, [
            'ip'    => 'string',
            'host'  => 'string',
            'descr' => 'string',
        ]);
        if ($this->has('token')) {
            $v = $this->sanitizeRule($sanitize, $this->get('token'), 'alphaNum');
            $this->replace(array_replace($this->all(), ['token' => $v]));
        }
        $rules = $token->validationRules();
        if (!$this->has('token')) {
            unset($rules['token']);
        }
        return $rules;
    }
}
