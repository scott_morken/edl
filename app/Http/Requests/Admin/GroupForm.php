<?php

namespace App\Http\Requests\Admin;

use App\Contracts\Storage\Group;
use App\Contracts\Storage\Parser;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use Smorken\Sanitizer\Contracts\Sanitize;
use Smorken\Sanitizer\Traits\SanitizeRequest;

class GroupForm extends FormRequest
{

    use SanitizeRequest;

    public function rules(Group $group, Parser $parser, Sanitize $sanitize)
    {
        $this->sanitize($sanitize, $this, [
            'active'      => 'bool',
            'description' => 'string',
            'parser'      => function ($value) {
                return preg_replace('/[^A-z0-9\\_]/', '', $value);
            },
        ]);
        $rules = $group->validationRules();
        $pr = Arr::get($rules, 'parser', []);
        $pr[] = new \App\Services\Validators\Parser($parser);
        $rules['parser'] = $pr;
        return $rules;
    }
}
