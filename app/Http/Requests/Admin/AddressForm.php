<?php

namespace App\Http\Requests\Admin;

use App\Contracts\Storage\Address;
use Illuminate\Foundation\Http\FormRequest;
use Smorken\Sanitizer\Contracts\Sanitize;
use Smorken\Sanitizer\Traits\SanitizeRequest;

class AddressForm extends FormRequest
{

    use SanitizeRequest;

    public function rules(Address $address, Sanitize $sanitize)
    {
        $this->sanitize($sanitize, $this, [
            'address'     => function ($value) {
                return preg_replace('/[^0-9:\/\.]/', '', $value);
            },
            'active'      => 'bool',
            'description' => 'string',
        ]);
        return $address->validationRules();
    }
}
