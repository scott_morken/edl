<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 8:51 AM
 */

namespace App\Storage\Eloquent;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Smorken\Support\Contracts\Filter;

class Address extends Base implements \App\Contracts\Storage\Address
{

    /**
     * @return \Illuminate\Support\Collection
     */
    public function active()
    {
        $key = 'addresses.active';
        return Cache::remember(
            $key,
            10,
            function () {
                return $this->getModel()
                            ->newQuery()
                            ->orderDefault()
                            ->activeIs(1)
                            ->get();
            }
        );
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getActiveCounts()
    {
        $key = 'addresses.active.counts';
        return Cache::remember(
            $key,
            60,
            function () {
                return $this->getModel()
                            ->select('active', DB::raw('count(*) as active_count'))
                            ->groupBy('active')
                            ->get();
            }
        );
    }

    /**
     * @param  \Smorken\Support\Contracts\Filter  $filter
     * @param  int  $per_page
     * @return \Illuminate\Support\Collection
     */
    public function getByFilter(Filter $filter, $per_page = 20): \Traversable
    {
        $allowed = [
            'address'  => 'filterAddress',
            'active'   => 'filterActive',
            'group_id' => 'filterGroupId',
        ];
        $q = $this->getModel()
                  ->newQuery()
                  ->with('group')
                  ->orderDefault();
        foreach ($filter->toArray() as $k => $v) {
            $method = Arr::get($allowed, $k);
            if ($method) {
                $q = $this->$method($q, $v);
            }
        }
        return $this->limitOrPaginate($q, $per_page);
    }

    /**
     * @return int
     */
    public function removeExpired()
    {
        return $this->getModel()
                    ->newQuery()
                    ->remove()
                    ->delete();
    }

    protected function filterActive($q, $v)
    {
        if (strlen($v)) {
            $q = $q->activeIs($v);
        }
        return $q;
    }

    protected function filterAddress($q, $v)
    {
        if (strlen($v)) {
            $q = $q->addressLike($v);
        }
        return $q;
    }

    protected function filterGroupId($q, $v)
    {
        if (strlen($v) && $v !== '*') {
            $q = $q->groupIdIs($v);
        }
        return $q;
    }
}
