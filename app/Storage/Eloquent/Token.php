<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/21/18
 * Time: 12:30 PM
 */

namespace App\Storage\Eloquent;

use Illuminate\Support\Facades\Cache;
use Smorken\Support\Contracts\Filter;

class Token extends Base implements \App\Contracts\Storage\Token
{

    /**
     * @param  \Smorken\Support\Contracts\Filter  $filter
     * @param  int  $per_page
     * @return \Illuminate\Support\Collection
     */
    public function getByFilter(Filter $filter, $per_page = 20): \Traversable
    {
        $allowed = [
            'ip'   => 'filterIp',
            'host' => 'filterHost',
        ];
        $q = $this->getModel()
                  ->newQuery()
                  ->defaultOrder();
        foreach ($filter->toArray() as $m => $v) {
            if (array_key_exists($m, $allowed)) {
                $method = $allowed[$m];
                $q = $this->$method($q, $v);
            }
        }
        return $this->limitOrPaginate($q, $per_page);
    }

    /**
     * @param      $ip
     * @param      $token
     * @param null $host
     * @return \App\Contracts\Models\Token|null
     */
    public function getToken($ip, $token, $host = null)
    {
        $key = sprintf('%s-%s-%s', $ip, $token, $host);
        return Cache::remember(
            $key,
            240,
            function () use ($ip, $token, $host) {
                $q = $this->getModel()
                          ->newQuery()
                          ->ipIs($ip)
                          ->tokenIs($token);
                if ($host) {
                    $q->hostIs($host);
                }
                $token = $q->first();
                if ($token && $token->host == $host) {
                    return $token;
                }
                return null;
            }
        );
    }

    protected function filterHost($query, $value)
    {
        if (strlen($value)) {
            if ($value === '-') {
                $value = '';
            }
            $query = $query->hostIs($value);
        }
        return $query;
    }

    protected function filterIp($query, $value)
    {
        if (strlen($value)) {
            $query = $query->ipIs($value);
        }
        return $query;
    }
}
