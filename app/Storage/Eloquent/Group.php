<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 2:14 PM
 */

namespace App\Storage\Eloquent;

use Illuminate\Support\Facades\Cache;

class Group extends Base implements \App\Contracts\Storage\Group
{

    /**
     * @param  bool  $addresses
     * @return \Illuminate\Support\Collection
     */
    public function active($addresses = true)
    {
        $key = 'groups.active';
        return Cache::remember($key, 10, function () use ($addresses) {
            $q = $this->getModel()
                      ->newQuery()
                      ->activeIs(1);
            if ($addresses) {
                $q->activeAddresses();
            }
            return $q->get();
        });
    }

    public function all(): \Traversable
    {
        return $this->getModel()
                    ->newQuery()
                    ->orderDefault()
                    ->get();
    }

    public function find($id)
    {
        return $this->getModel()
                    ->newQuery()
                    ->activeIs(1)
                    ->activeAddresses()
                    ->find($id);
    }
}
