<?php

namespace App\Storage\Config;

use Smorken\Storage\Abstraction\Config;

class Parser extends Config implements \App\Contracts\Storage\Parser
{

    /**
     * @return array
     */
    public function allowedToArray()
    {
        $allowed = [];
        foreach ($this->all() as $model) {
            $allowed[] = $model->id;
        }
        return $allowed;
    }
}
