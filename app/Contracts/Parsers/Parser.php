<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 11:54 AM
 */

namespace App\Contracts\Parsers;

use Illuminate\Support\Collection;

interface Parser
{

    /**
     * @param \Illuminate\Support\Collection $collection
     * @return string
     */
    public function parse(Collection $collection);
}
