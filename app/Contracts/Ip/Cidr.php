<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/14/17
 * Time: 2:03 PM
 */

namespace App\Contracts\Ip;

interface Cidr
{

    /**
     * @param bool $as_long
     * @return array
     */
    public function asArray($as_long = false);

    /**
     * @param bool $as_long
     * @return string|int
     */
    public function getBroadcast($as_long = false);

    /**
     * @param bool $as_long
     * @return string|int
     */
    public function getFirst($as_long = false);

    /**
     * @param bool $as_long
     * @return string|int
     */
    public function getIp($as_long = false);

    /**
     * @param bool $as_long
     * @return string|int
     */
    public function getLast($as_long = false);

    /**
     * @param bool $as_long
     * @return string|int
     */
    public function getNetmask($as_long = false);

    /**
     * @return int
     */
    public function getNetmaskInt();

    /**
     * @param bool $as_long
     * @return string|int
     */
    public function getNetwork($as_long = false);

    /**
     * @param bool $as_long
     * @return \Generator
     */
    public function getRange($as_long = false);

    /**
     * @param $subnet
     * @return bool
     */
    public function inSubnet($subnet);

    /**
     * @param $subnet
     * @return Cidr
     */
    public function newInstance($subnet);

    /**
     * @return bool
     * @throws \App\Services\Ip\CidrException
     */
    public function validate();
}
