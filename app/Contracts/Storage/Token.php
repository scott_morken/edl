<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/21/18
 * Time: 12:28 PM
 */

namespace App\Contracts\Storage;

interface Token
{

    /**
     * @param      $ip
     * @param      $token
     * @param null $host
     * @return \App\Contracts\Models\Token|null
     */
    public function getToken($ip, $token, $host = null);
}
