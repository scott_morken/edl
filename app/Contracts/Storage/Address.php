<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 8:38 AM
 */

namespace App\Contracts\Storage;

interface Address
{

    /**
     * @return \Illuminate\Support\Collection
     */
    public function active();

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getActiveCounts();

    /**
     * @return int
     */
    public function removeExpired();
}
