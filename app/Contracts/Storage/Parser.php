<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 1:52 PM
 */

namespace App\Contracts\Storage;

interface Parser
{

    /**
     * @return array
     */
    public function allowedToArray();
}
