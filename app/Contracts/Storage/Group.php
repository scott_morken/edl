<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 1:51 PM
 */

namespace App\Contracts\Storage;

interface Group
{

    /**
     * @param bool $addresses
     * @return \Illuminate\Support\Collection
     */
    public function active($addresses = true);
}
