<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/18/18
 * Time: 7:16 AM
 */

namespace App\Contracts;

use Psr\Log\LoggerInterface;

interface AccessLog extends LoggerInterface
{

}
