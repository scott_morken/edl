<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 12:40 PM
 */

namespace App\Contracts\Refreshers;

interface Refresh
{

    public function getBackend();

    public function refresh();
}
