<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 12:52 PM
 */

namespace App\Contracts\Refreshers;

interface Factory
{

    /**
     * @param                                   $key
     * @param \App\Contracts\Refreshers\Refresh $refresh
     * @return mixed
     */
    public function add($key, Refresh $refresh);

    /**
     * @return array
     */
    public function all();

    /**
     * @param $key
     * @return \App\Contracts\Refreshers\Refresh
     */
    public function get($key);
}
