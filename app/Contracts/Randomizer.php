<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/21/18
 * Time: 12:50 PM
 */

namespace App\Contracts;

interface Randomizer
{

    /**
     * @param int $len
     * @return string
     */
    public function generate($len = 64);
}
