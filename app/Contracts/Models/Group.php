<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 1:47 PM
 */

namespace App\Contracts\Models;

/**
 * Interface Group
 *
 * @package App\Contracts\Models
 *
 * @property int                            $id
 * @property string                         $parser
 * @property string                         $description
 * @property bool                           $active
 *
 * @property \Illuminate\Support\Collection $addresses
 */
interface Group
{

    /**
     * @return \App\Contracts\Parsers\Parser
     */
    public function instantiateParser();

    /**
     * @return mixed
     */
    public function parseAddresses();
}
