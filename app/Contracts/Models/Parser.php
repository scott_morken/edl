<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 1:52 PM
 */

namespace App\Contracts\Models;

/**
 * Interface Parser
 *
 * @package App\Contracts\Models
 *
 * @property string $id
 * @property string $name
 */
interface Parser
{

    /**
     * @return \App\Contracts\Parsers\Parser
     */
    public function getInstance();
}
