<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/21/18
 * Time: 12:27 PM
 */

namespace App\Contracts\Models;

/**
 * Interface Token
 *
 * @package App\Contracts\Models
 *
 * @property int         $id
 * @property string      $ip
 * @property string      $token
 * @property string|null $host
 * @property string|null $descr
 */
interface Token
{

}
