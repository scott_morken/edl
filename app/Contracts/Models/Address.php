<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 8:26 AM
 */

namespace App\Contracts\Models;

/**
 * Interface Address
 *
 * @package App\Contracts\Models
 *
 * @property int                         $id
 * @property int                         $group_id
 * @property string                      $address
 * @property string                      $description
 * @property bool                        $active
 * @property string|null                 $remove_at
 *
 * @property \App\Contracts\Ip\Cidr      $cidr
 *
 * @property \App\Contracts\Models\Group $group
 */
interface Address
{

}
