<?php

namespace App\Providers;

use App\Contracts\Ip\Cidr;
use App\Contracts\Randomizer;
use App\Contracts\Refreshers\Factory;
use App\Services\Randomizers\Core;
use App\Services\Refreshers\Pan;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Randomizer::class, function ($app) {
            return new Core();
        });

        $this->app->bind(Cidr::class, function ($app) {
            return new \App\Services\Ip\Cidr();
        });

        $this->bindRefreshers();
    }

    protected function bindRefreshers()
    {
        $this->app->bind(Factory::class, function ($app) {
            $f = new \App\Services\Refreshers\Factory();
            $pan = new Pan(new Client(), $app['config']->get('addresses.refreshers.pan', []));
            $f->add('pan', $pan);
//                $none = new None();
//                $f->add('none', $none);
            return $f;
        });
    }
}
