<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/healthz', function () {
    return \Illuminate\Support\Facades\Response::make('ok', 200);
});

Route::get('/', 'HomeController@index');

Route::group([
    'prefix'     => 'admin',
    'middleware' => ['auth', 'can:role-admin'],
    'namespace'  => 'Admin',
], function () {

    Route::group([
        'prefix'    => 'address',
        'namespace' => 'Address',
    ], function () {
        \Smorken\Support\Routes::create('Controller', [], ['get|refresh' => 'refresh']);
    });

    Route::group([
        'prefix'    => 'group',
        'namespace' => 'Group',
    ], function () {
        \Smorken\Support\Routes::create();
    });

    Route::group([
        'prefix'    => 'token',
        'namespace' => 'Token',
    ], function () {
        \Smorken\Support\Routes::create();
    });
});
