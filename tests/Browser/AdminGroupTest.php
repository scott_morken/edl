<?php

namespace Tests\App\Browser;

use App\Models\Eloquent\Group;
use Laravel\Dusk\Browser;
use Smorken\Auth\Proxy\Models\Eloquent\User;
use Tests\App\DuskTestCase;

class AdminGroupTest extends DuskTestCase
{

    /**
     * @test
     * @throws \Throwable
     */
    public function testBaseRouteWithAdminUser()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/group')
                    ->assertSee('Group Administration')
                    ->assertSee('No records found');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testBaseRouteWithoutAdminUser()
    {
        $user = factory(User::class)->create([
            'id'         => 2,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/group')
                    ->assertSee("You don't have permission");
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testCreateNewRecord()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/group')
                    ->clickLink('New')
                    ->assertPathIs('/admin/group/create')
                    ->type('description', 'Test Create')
                    ->check('active')
                    ->press('Save')
                    ->assertPathIs('/admin/group')
                    ->assertSee('Test Create');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testCreateNewRecordFailsValidation()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/group')
                    ->clickLink('New')
                    ->assertPathIs('/admin/group/create')
                    ->press('Save')
                    ->assertPathIs('/admin/group/create')
                    ->assertSee('field is required');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testDeleteExistingRecord()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $group = factory(Group::class)->create();
            $browser->loginAs($user)
                    ->visit('/admin/group')
                    ->assertSee($group->description)
                    ->clickLink('delete')
                    ->assertPathIs('/admin/group/delete/'.$group->id)
                    ->assertSee($group->description)
                    ->press('Delete')
                    ->assertPathIs('/admin/group')
                    ->assertDontSee($group->description);
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testDeleteMissingRecord()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/group/delete/2')
                    ->assertSee("The resource you were trying to reach");
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testUpdateExistingRecord()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $group = factory(Group::class)->create();
            $browser->loginAs($user)
                    ->visit('/admin/group')
                    ->assertSee($group->description)
                    ->clickLink('update')
                    ->assertPathIs('/admin/group/update/'.$group->id)
                    ->assertInputValue('description', $group->description)
                    ->assertChecked('active')
                    ->type('description', 'Test Update')
                    ->press('Save')
                    ->assertPathIs('/admin/group')
                    ->assertSee('Test Update');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testUpdateExistingRecordFailsValidation()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $group = factory(Group::class)->create();
            $browser->loginAs($user)
                    ->visit('/admin/group')
                    ->assertSee($group->description)
                    ->clickLink('update')
                    ->assertPathIs('/admin/group/update/'.$group->id)
                    ->assertInputValue('description', $group->description)
                    ->assertChecked('active')
                    ->clear('description')
                    ->press('Save')
                    ->assertPathIs('/admin/group/update/'.$group->id)
                    ->assertSee('field is required');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testUpdateMissingRecord()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $group = factory(Group::class)->create();
            $browser->loginAs($user)
                    ->visit('/admin/group/update/2')
                    ->assertSee("The resource you were trying to reach");
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testViewExistingRecord()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $group = factory(Group::class)->create();
            $browser->loginAs($user)
                    ->visit('/admin/group')
                    ->assertSee($group->description)
                    ->clickLink('1')
                    ->assertPathIs('/admin/group/view/'.$group->id)
                    ->assertSee($group->description);
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testViewMissingRecord()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/group/view/2')
                    ->assertSee("The resource you were trying to reach");
        });
    }
}
