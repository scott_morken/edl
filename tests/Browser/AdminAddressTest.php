<?php

namespace Tests\App\Browser;

use App\Models\Eloquent\Address;
use App\Models\Eloquent\Group;
use Laravel\Dusk\Browser;
use Smorken\Auth\Proxy\Models\Eloquent\User;
use Tests\App\DuskTestCase;

class AdminAddressTest extends DuskTestCase
{

    /**
     * @test
     * @throws \Throwable
     */
    public function testBaseRouteWithAdminUser()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/address')
                    ->assertSee('IP Administration')
                    ->assertSee('No records found');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testBaseRouteWithoutAdminUser()
    {
        $user = factory(User::class)->create([
            'id'         => 2,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/address')
                    ->assertSee("You don't have permission");
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testCreateNewRecord()
    {
        $this->ensureGroup();
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/address')
                    ->clickLink('New')
                    ->assertPathIs('/admin/address/create')
                    ->type('address', '1.2.3.4/32')
                    ->type('description', 'Test Create')
                    ->check('active')
                    ->press('Save')
                    ->assertPathIs('/admin/address')
                    ->assertSee('1.2.3.4/32');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testCreateNewRecordFailsValidation()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/address')
                    ->clickLink('New')
                    ->assertPathIs('/admin/address/create')
                    ->press('Save')
                    ->assertPathIs('/admin/address/create')
                    ->assertSee('field is required');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testCreateNewRecordWithPrivateSubnetFails()
    {
        $this->ensureGroup();
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/address')
                    ->clickLink('New')
                    ->assertPathIs('/admin/address/create')
                    ->type('address', '172.22.0.1/24')
                    ->type('description', 'Test Create')
                    ->check('active')
                    ->press('Save')
                    ->assertPathIs('/admin/address/create')
                    ->assertSee('The address must be a public IP');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testDeleteExistingRecord()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $group = $this->ensureGroup();
            $model = factory(Address::class)->create(['group_id' => $group->id]);
            $browser->loginAs($user)
                    ->visit('/admin/address')
                    ->assertSee($model->address)
                    ->clickLink('delete')
                    ->assertPathIs('/admin/address/delete/'.$model->id)
                    ->assertSee($model->address)
                    ->press('Delete')
                    ->assertPathIs('/admin/address')
                    ->assertDontSee($model->address);
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testDeleteMissingRecord()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/address/delete/2')
                    ->assertSee("The resource you were trying to reach");
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testUpdateExistingRecord()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $group = $this->ensureGroup();
            $model = factory(Address::class)->create(['group_id' => $group->id]);
            $browser->loginAs($user)
                    ->visit('/admin/address')
                    ->assertSee($model->address)
                    ->clickLink('update')
                    ->assertPathIs('/admin/address/update/'.$model->id)
                    ->assertInputValue('address', $model->address)
                    ->assertInputValue('description', $model->description)
                    ->assertChecked('active')
                    ->type('description', 'Test Update')
                    ->press('Save')
                    ->assertPathIs('/admin/address')
                    ->assertSee('Test Update');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testUpdateExistingRecordFailsValidation()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $group = $this->ensureGroup();
            $model = factory(Address::class)->create(['group_id' => $group->id]);
            $browser->loginAs($user)
                    ->visit('/admin/address')
                    ->assertSee($model->address)
                    ->clickLink('update')
                    ->assertPathIs('/admin/address/update/'.$model->id)
                    ->assertInputValue('address', $model->address)
                    ->assertInputValue('description', $model->description)
                    ->assertChecked('active')
                    ->clear('address')
                    ->press('Save')
                    ->assertPathIs('/admin/address/update/'.$model->id)
                    ->assertSee('address field is required');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testUpdateMissingRecord()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = factory(Address::class)->create();
            $browser->loginAs($user)
                    ->visit('/admin/address/update/2')
                    ->assertSee("The resource you were trying to reach");
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testViewExistingRecord()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $group = $this->ensureGroup();
            $model = factory(Address::class)->create(['group_id' => $group->id]);
            $browser->loginAs($user)
                    ->visit('/admin/address')
                    ->assertSee($model->address)
                    ->clickLink('1')
                    ->assertPathIs('/admin/address/view/'.$model->id)
                    ->assertSee($model->address)
                    ->assertSee($group->description);
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testViewMissingRecord()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/address/view/2')
                    ->assertSee("The resource you were trying to reach");
        });
    }

    protected function ensureGroup()
    {
        return factory(Group::class)->create();
    }
}
