<?php

namespace Tests\App\Browser;

use App\Models\Eloquent\Address;
use App\Models\Eloquent\Group;
use App\Models\Eloquent\Token;
use Laravel\Dusk\Browser;
use Tests\App\DuskTestCase;

class ListTest extends DuskTestCase
{

    public function test1NoListAndNotAuthorized()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(route('list', ['id' => 1]))
                    ->assertSee('Unauthorized');
        });
    }

    public function test2NoGroupAndNoListAndAuthorized()
    {
        $this->browse(function (Browser $browser) {
            $token = $this->ensureToken();
            $browser->visit(route('list', ['id' => 1, 'token' => $token->token]))
                    ->assertSee('1 is not a valid group');
        });
    }

    public function test3GroupsExistsNoListAndAuthorized()
    {
        $group = factory(Group::class)->create();
        $this->browse(function (Browser $browser) {
            $token = $this->ensureToken();
            $browser->visit(route('list', ['id' => 1, 'token' => $token->token]))
                    ->assertSourceHas('<body></body>');
        });
    }

    public function test4GroupsExistsWithListAndAuthorized()
    {
        $group = factory(Group::class)->create();
        $address = factory(Address::class)->create(['group_id' => $group->id]);
        $this->browse(function (Browser $browser) use ($address) {
            $token = $this->ensureToken();
            $browser->visit(route('list', ['id' => 1, 'token' => $token->token]))
                    ->assertSee(sprintf('%s %d: %s', $address->ip, $address->id, $address->description));
        });
    }

    protected function ensureToken()
    {
        return factory(Token::class)->create(['ip' => '172.44.0.4']);
    }
}
