<?php

namespace Tests\App\Browser;

use App\Models\Eloquent\Address;
use Laravel\Dusk\Browser;
use Tests\App\DuskTestCase;

class HomeTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testHomeRouteNoAddresses()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('No addresses found.');
        });
    }

    public function testHomeRouteWithAddresses()
    {
        factory(Address::class, 3)->create();
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('Active: 3');
        });
    }

    public function testHomeRouteWithAddressesIgnoresInactive()
    {
        factory(Address::class, 2)->create();
        factory(Address::class)->create(['active' => 0]);
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('Active: 2');
        });
    }
}
