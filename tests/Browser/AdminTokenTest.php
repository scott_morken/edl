<?php

namespace Tests\App\Browser;

use App\Models\Eloquent\Token;
use Laravel\Dusk\Browser;
use Smorken\Auth\Proxy\Models\Eloquent\User;
use Tests\App\DuskTestCase;

class AdminTokenTest extends DuskTestCase
{

    /**
     * @test
     * @throws \Throwable
     */
    public function testBaseRouteWithAdminUser()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/token')
                    ->assertSee('Token Administration')
                    ->assertSee('No records found');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testBaseRouteWithoutAdminUser()
    {
        $user = factory(User::class)->create([
            'id'         => 2,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/token')
                    ->assertSee("You don't have permission");
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testCreateNewRecord()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/token')
                    ->clickLink('New')
                    ->assertPathIs('/admin/token/create')
                    ->type('ip', '1.1.1.1')
                    ->type('descr', 'Test Create')
                    ->press('Save')
                    ->assertPathIs('/admin/token')
                    ->assertSee('1.1.1.1');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testCreateNewRecordFailsValidation()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/token')
                    ->clickLink('New')
                    ->assertPathIs('/admin/token/create')
                    ->press('Save')
                    ->assertPathIs('/admin/token/create')
                    ->assertSee('ip field is required');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testDeleteExistingRecord()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $group = factory(Token::class)->create();
            $browser->loginAs($user)
                    ->visit('/admin/token')
                    ->assertSee($group->ip)
                    ->clickLink('delete')
                    ->assertPathIs('/admin/token/delete/'.$group->id)
                    ->assertSee($group->ip)
                    ->press('Delete')
                    ->assertPathIs('/admin/token')
                    ->assertDontSee($group->ip);
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testDeleteMissingRecord()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/token/delete/2')
                    ->assertSee("The resource you were trying to reach");
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testUpdateExistingRecord()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $token = factory(Token::class)->create();
            $browser->loginAs($user)
                    ->visit('/admin/token')
                    ->assertSee($token->ip)
                    ->clickLink('update')
                    ->assertPathIs('/admin/token/update/'.$token->id)
                    ->assertInputValue('ip', $token->ip)
                    ->assertInputValue('token', $token->token)
                    ->assertInputValue('descr', $token->descr)
                    ->type('descr', 'Test Update')
                    ->press('Save')
                    ->assertPathIs('/admin/token')
                    ->assertSee($token->ip);
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testUpdateExistingRecordFailsValidation()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $token = factory(Token::class)->create();
            $browser->loginAs($user)
                    ->visit('/admin/token')
                    ->assertSee($token->ip)
                    ->clickLink('update')
                    ->assertPathIs('/admin/token/update/'.$token->id)
                    ->assertInputValue('ip', $token->ip)
                    ->clear('ip')
                    ->press('Save')
                    ->assertPathIs('/admin/token/update/'.$token->id)
                    ->assertSee('ip field is required');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testUpdateMissingRecord()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/token/update/2')
                    ->assertSee("The resource you were trying to reach");
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testViewExistingRecord()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $token = factory(Token::class)->create();
            $browser->loginAs($user)
                    ->visit('/admin/token')
                    ->assertSee($token->ip)
                    ->clickLink('1')
                    ->assertPathIs('/admin/token/view/'.$token->id)
                    ->assertSee($token->ip);
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testViewMissingRecord()
    {
        $user = factory(User::class)->create([
            'id'         => 1,
            'username'   => 'foobar',
            'email'      => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name'  => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/token/view/2')
                    ->assertSee("The resource you were trying to reach");
        });
    }
}
