<?php

namespace Tests\App;

use Dotenv\Dotenv;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;
use Illuminate\Support\Facades\Auth;
use Laravel\Dusk\TestCase as BaseTestCase;
use Mockery as m;
use Smorken\Auth\Proxy\AuthProvider;
use Smorken\Auth\Proxy\Common\Contracts\Provider;
use Smorken\Auth\Proxy\Contracts\Models\User;
use Smorken\Roles\Models\Eloquent\RoleUser;
use Symfony\Component\HttpFoundation\File\Exception\CannotWriteFileException;

abstract class DuskTestCase extends BaseTestCase
{
    use CreatesApplication;

    protected static $migrationsRun = false;

    protected static $seedsRun = false;

    protected $retries = 5;

    protected $runMigrations = true;

    protected $shouldSeed = true;

    protected $shouldDelete = true;

    public static function basePath($path = '')
    {
        return __DIR__.'/../'.($path ? DIRECTORY_SEPARATOR.$path : $path);
    }

    /**
     * Prepare for Dusk test execution.
     *
     * @beforeClass
     * @return void
     */
    public static function prepare()
    {
        static::startChromeDriver();
    }

    /**
     * Define hooks to migrate the database before and after each test.
     *
     * @return void
     */
    public function runDatabaseMigrations()
    {
        if ($this->runMigrations === false) {
            return;
        }
        if (static::$migrationsRun === false) {
            $this->artisan('migrate:fresh');
            $this->createTablesForTest();
            static::$migrationsRun = true;
        }

        $this->beforeApplicationDestroyed(function () {
            if ($this->shouldDelete) {
                $this->artisan('migrate:fresh');
                $this->deleteTablesForTest();
            }
        });
        $this->seedTables();
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->runDatabaseMigrations();
        $this->beforeApplicationDestroyed(function () {
            self::tryCopy(self::basePath('.env.backup'), self::basePath('.env'));
        });
    }

    public static function setUpBeforeClass(): void
    {
        if (!file_exists(self::basePath('.env.original'))) {
            self::tryCopy(self::basePath('.env'), self::basePath('.env.original'));
        }
        self::tryCopy(self::basePath('.env'), self::basePath('.env.backup'));
        self::tryCopy(self::basePath('.env.dusk.local'), self::basePath('.env'));
        $dotenv = Dotenv::create(self::basePath());
        $dotenv->overload();
        parent::setUpBeforeClass();
    }

    protected function bindMockAuthProvider($detail = true)
    {
        if ($detail) {
            $provider = m::mock(Provider::class);
            $userProvider = m::mock(\Smorken\Auth\Proxy\Contracts\Storage\User::class);
            $authprovider = new AuthProvider($provider, $userProvider);
        } else {
            $authprovider = m::mock(AuthProvider::class);
        }
        Auth::provider('proxy', function ($app, array $config) use ($authprovider) {
            return $authprovider;
        });
        if ($detail) {
            return [$provider, $userProvider];
        }
        return [$authprovider];
    }

    protected function checkEndpoints(array $urls)
    {
        foreach ($urls as $url) {
            if (!$this->waitForEndpoint($url['url'], $url['callback'])) {
                throw new \Exception($url['url'].' is not responding.');
            }
        }
    }

    protected function createTablesForTest()
    {
        return;
    }

    protected function deleteTablesForTest()
    {
        return;
    }

    /**
     * Create the RemoteWebDriver instance.
     *
     * @return \Facebook\WebDriver\Remote\RemoteWebDriver
     * @throws \Exception
     */
    protected function driver()
    {
        $this->checkEndpoints($this->getUrls());
        $url = env('DUSK_BROWSER_URL', 'http://localhost:9515');
        $options = (new ChromeOptions)->addArguments([
            '--disable-gpu',
            '--headless',
            '--window-size=1920,1080',
            '--ignore-ssl-errors',
            '--no-sandbox',
            '--whitelisted-ips',
        ]);
        return RemoteWebDriver::create($url, DesiredCapabilities::chrome()
                                                                ->setCapability(ChromeOptions::CAPABILITY, $options)
                                                                ->setCapability(WebDriverCapabilityType::ACCEPT_SSL_CERTS,
                                                                    true)
                                                                ->setCapability('acceptInsecureCerts', true));
    }

    protected function getResponse($url)
    {
        $c = curl_init();
        curl_setopt_array($c, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL            => $url,
        ]);
        $r = curl_exec($c);
        curl_close($c);
        return $r;
    }

    protected function getUrls()
    {
        return [
            [
                'url'      => env('DUSK_BROWSER_URL', 'http://localhost:9515').'/status',
                'callback' => function ($r) {
                    $response = json_decode($r, true);
                    if ($response && isset($response['value']['ready']) && $response['value']['ready'] === true) {
                        return true;
                    }
                    return false;
                },
            ],
            [
                'url'      => env('APP_URL', 'http://web:8000'),
                'callback' => function ($r) {
                    return $r !== false;
                },
            ],
        ];
    }

    protected function mockAuth(
        $user_data = ['id' => 1, 'first_name' => 'foo', 'last_name' => 'bar', 'email' => 'foomail@example.org'],
        $acting_as = true
    ) {
        $user = factory(User::class)->make($user_data);
        foreach ($user_data as $k => $v) {
            $user->$k = $v;
        }
        if ($acting_as) {
            $this->actingAs($user);
        }
        return $user;
    }

    protected function seedForTest()
    {
        return;
    }

    protected function seedTables()
    {
        if ($this->shouldSeed) {
            $this->seed('TestSeeder');
            if (class_exists(RoleUser::class)) {
                RoleUser::create(['role_id' => 1, 'user_id' => 1]);
            }
            if (!self::$seedsRun) {
                $this->seedForTest();
                self::$seedsRun = true;
            }
        }
    }

    protected static function tryCopy($from, $to)
    {
        $continue = true;
        if (file_exists($to)) {
            $fromstr = file_get_contents($from);
            $tostr = file_get_contents($to);
            $continue = $fromstr !== $tostr;
        }
        if ($continue) {
            if (!copy($from, $to)) {
                throw new CannotWriteFileException("Cannot write [$from] to [$to].");
            }
        }
    }

    protected function waitForEndpoint($url, callable $valid, $tries = 0)
    {
        $wait = $tries < $this->retries ? true : false;
        $r = $this->getResponse($url);
        if ($r) {
            if ($valid($r)) {
                return true;
            } else {
                $r = false;
            }
        }
        if ((!$r) && $wait) {
            sleep(1);
            return $this->waitForEndpoint($url, $valid, ++$tries);
        }
        return false;
    }
}
