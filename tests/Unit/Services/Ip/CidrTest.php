<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/14/17
 * Time: 2:35 PM
 */

namespace Tests\App\Unit\Services\Ip;

use App\Services\Ip\Cidr;
use App\Services\Ip\CidrException;
use PHPUnit\Framework\TestCase;

class CidrTest extends TestCase
{

    public function testGetBroadcastMultiSubnet()
    {
        $sut = $this->getSut('10.10.1.1/16');
        $this->assertEquals('10.10.255.255', $sut->getBroadcast());
    }

    public function testGetBroadcastOddSubnet()
    {
        $sut = $this->getSut('10.10.1.1/32');
        $this->assertEquals('10.10.1.1', $sut->getBroadcast());
    }

    public function testGetBroadcastSingleSubnet()
    {
        $sut = $this->getSut('10.10.1.1/24');
        $this->assertEquals('10.10.1.255', $sut->getBroadcast());
    }

    public function testGetFirstAndLastMultiSubnet()
    {
        $sut = $this->getSut('10.10.1.1/16');
        $this->assertEquals('10.10.0.1', $sut->getFirst());
        $this->assertEquals('10.10.255.254', $sut->getLast());
    }

    public function testGetFirstMultiSubnet()
    {
        $sut = $this->getSut('10.10.1.1/16');
        $this->assertEquals('10.10.0.1', $sut->getFirst());
    }

    public function testGetFirstSingleSubnet()
    {
        $sut = $this->getSut('10.10.1.1/24');
        $this->assertEquals('10.10.1.1', $sut->getFirst());
    }

    public function testGetNetworkMultiSubnet()
    {
        $sut = $this->getSut('10.10.1.1/16');
        $this->assertEquals('10.10.0.0', $sut->getNetwork());
    }

    public function testGetNetworkSingleSubnet()
    {
        $sut = $this->getSut('127.0.0.10/24');
        $this->assertEquals('127.0.0.0', $sut->getNetwork());
    }

    public function testGetRangeNormal()
    {
        $sut = $this->getSut('10.10.1.1/24');
        $r = [];
        foreach ($sut->getRange() as $ip) {
            $r[] = $ip;
        }
        $this->assertCount(254, $r);
    }

    public function testGetRangeWithOddSubnetIsEmpty()
    {
        $sut = $this->getSut('10.10.1.1/32');
        $r = [];
        foreach ($sut->getRange() as $ip) {
            $r[] = $ip;
        }
        $this->assertCount(0, $r);
    }

    public function testInSubnetIsTrue()
    {
        $sut = $this->getSut('127.0.0.1/24');
        $this->assertTrue($sut->inSubnet('127.0.0.0/16'));
    }

    public function testInZeroSubnetIsFalse()
    {
        $sut = $this->getSut('127.0.2.1/24');
        $this->assertFalse($sut->inSubnet('0.0.0.0/8'));
    }

    public function testInvalidIpIsException()
    {
        $this->expectException(CidrException::class);
        $sut = $this->getSut('foo/24');
    }

    public function testInvalidMaskAsZeroIsException()
    {
        $this->expectException(CidrException::class);
        $sut = $this->getSut('127.0.0.1/0');
    }

    public function testInvalidMaskIsException()
    {
        $this->expectException(CidrException::class);
        $sut = $this->getSut('127.0.0.1/500');
    }

    public function testNewInstanceIsNew()
    {
        $sut = $this->getSut('10.10.1.1/24');
        $new = $sut->newInstance('192.168.0.1/24');
        $expected = [
            'ip'        => '192.168.0.1',
            'netmask'   => '255.255.255.0',
            'network'   => '192.168.0.0',
            'broadcast' => '192.168.0.255',
        ];
        $this->assertEquals($expected, $new->asArray());
    }

    public function testNoMaskIsException()
    {
        $this->expectException(CidrException::class);
        $sut = $this->getSut('127.0.0.1');
    }

    public function testNotInSubnetIsFalse()
    {
        $sut = $this->getSut('127.0.2.1/24');
        $this->assertFalse($sut->inSubnet('127.0.0.0/24'));
    }

    public function testOneAddressInSubnetIsTrue()
    {
        $sut = $this->getSut('127.0.0.200/32');
        $this->assertTrue($sut->inSubnet('127.0.0.0/24'));
    }

    public function testOneAddressIsSubnetIsTrue()
    {
        $sut = $this->getSut('127.0.0.1/32');
        $this->assertTrue($sut->inSubnet('127.0.0.1/24'));
    }

    /**
     * @return Cidr
     */
    protected function getSut($subnet)
    {
        return new Cidr($subnet);
    }
}
