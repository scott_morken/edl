<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/17/18
 * Time: 10:18 AM
 */

namespace Tests\App\Unit\Services\Validators;

use App\Services\Ip\Cidr;
use App\Services\Validators\Address;
use PHPUnit\Framework\TestCase;

class AddressTest extends TestCase
{

    public function testInvalidExclusionIsFalse()
    {
        $sut = $this->getSut(['172.0.0.1/65']);
        $this->assertFalse($sut->passes('ip', '172.0.1.1/24'));
        $this->assertEquals('Invalid exclusion subnet [172.0.0.1/65].', $sut->message());
    }

    public function testExcludedIsFalse()
    {
        $sut = $this->getSut(['172.0.0.1/16']);
        $this->assertFalse($sut->passes('ip', '172.0.1.1/24'));
        $this->assertEquals('The :attribute must not be in an excluded subnet [172.0.0.1/16].', $sut->message());
    }

    public function testPassesAllIsTrue()
    {
        $sut = $this->getSut(['172.0.0.1/16']);
        $this->assertTrue($sut->passes('ip', '172.1.0.1/24'));
    }

    public function testInvalidMaskIsFalse()
    {
        $sut = $this->getSut();
        $this->assertFalse($sut->passes('ip', '10.1.1.1/34'));
        $this->assertEquals('The :attribute must be a valid subnet mask.', $sut->message());
    }

    public function testPrivateIsFalse()
    {
        $sut = $this->getSut();
        $this->assertFalse($sut->passes('ip', '10.0.0.1/24'));
        $this->assertEquals('The :attribute must be a public IP.', $sut->message());
    }

    public function testReservedIsFalse()
    {
        $sut = $this->getSut();
        $this->assertFalse($sut->passes('ip', '127.0.0.1/32'));
        $this->assertEquals('The :attribute must be a public IP.', $sut->message());
    }

    protected function getSut($exclusions = [])
    {
        return new Address(new Cidr(), $exclusions);
    }
}
