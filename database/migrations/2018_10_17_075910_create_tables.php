<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTables extends Migration
{
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tables = ['addresses', 'groups', 'tokens'];
        foreach ($tables as $t) {
            Schema::dropIfExists($t);
        }
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'addresses',
            function (Blueprint $t) {
                $t->increments('id');
                $t->integer('group_id')
                  ->unsigned();
                $t->ipAddress('address');
                $t->string('description', 128)
                  ->nullable();
                $t->boolean('active');
                $t->dateTime('remove_at')
                  ->nullable();
                $t->timestamps();

                $t->index('group_id', 'add_group_id_ndx');
                $t->index('address', 'add_address_ndx');
                $t->index('active', 'add_active_ndx');
                $t->index('remove_at', 'add_remove_at_ndx');
            }
        );

        Schema::create(
            'groups',
            function (Blueprint $t) {
                $t->increments('id');
                $t->string('parser', 128);
                $t->string('description', 128)
                  ->nullable();
                $t->boolean('active');
                $t->timestamps();

                $t->index('active', 'group_active_ndx');
            }
        );

        Schema::create(
            'tokens',
            function (Blueprint $t) {
                $t->increments('id');
                $t->ipAddress('ip');
                $t->string('token', 128);
                $t->string('host', 128)
                  ->nullable();
                $t->string('descr', 128)
                  ->nullable();
                $t->timestamps();

                $t->index('ip', 'tok_ip_ndx');
                $t->index('token', 'tok_token_ndx');
                $t->index('host', 'tok_host_ndx');
            }
        );
    }
}
