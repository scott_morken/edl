<?php

class TestSeeder extends \Illuminate\Database\Seeder
{

    public function run()
    {
        if (class_exists(RoleSeeder::class)) {
            $this->call(RoleSeeder::class);
        }
    }
}
