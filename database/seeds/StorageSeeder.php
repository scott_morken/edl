<?php

use Illuminate\Support\Facades\App;

abstract class StorageSeeder extends \Illuminate\Database\Seeder
{

    protected $provider_class;

    protected $provider;

    public function run()
    {
        $this->truncate();
        foreach ($this->getData() as $row) {
            $this->create($row);
        }
    }

    protected function create(array $attributes)
    {
        return $this->getModel()
                    ->create($attributes);
    }

    abstract protected function getData();

    protected function getModel()
    {
        $p = $this->getProvider();
        return $p->getModel();
    }

    protected function getProvider()
    {
        if (!$this->provider) {
            $this->provider = App::make($this->provider_class);
        }
        return $this->provider;
    }

    protected function truncate()
    {
        return $this->getModel()
                    ->truncate();
    }
}
