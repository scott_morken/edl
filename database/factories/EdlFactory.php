<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\Eloquent\Address::class, function (Faker $faker) {
    return [
        'group_id'    => $faker->randomNumber(2),
        'address'     => '1.2.3.4/24',
        'description' => implode(' ', $faker->words),
        'active'      => 1,
        'remove_at'   => null,
    ];
});

$factory->define(\App\Models\Eloquent\Group::class, function (Faker $faker) {
    return [
        'parser'      => \App\Services\Parsers\Edl::class,
        'description' => implode(' ', $faker->words),
        'active'      => 1,
    ];
});

$factory->define(\App\Models\Eloquent\Token::class, function (Faker $faker) {
    return [
        'ip'    => $faker->ipv4,
        'token' => \Illuminate\Support\Str::random(32),
        'host'  => null,
        'descr' => implode(' ', $faker->words),
    ];
});
